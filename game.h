#ifndef GAME_H
#define GAME_H
#include <iostream>
#include "user.h"
using namespace std;
class user;
class game{
public:
	game(string,int,string,int,int);
	void print();
	int get_score();
	int get_year();
	int get_rated_by();
	string get_name();
	void set_score(int,user);
private:
	string name;
	int year;
	string genre;
	int score;
	int rated_by;
};
#endif