#ifndef USER_H
#define USER_H
#include <iostream>
#include <vector>
#include "game.h"
using namespace std;
struct scrd{
	string game_name;
	bool scored;
	int score;
};
class game;
class user{
public:
	user();
	user(string,string,int,string,vector<game>);
	string get_name();
	bool login(string,string);
	void set_signed_in(bool);
	bool scored_before(string);
	int get_past_score(string);
	void set_past_score(string,int);
	void set_past_score(string);
	bool get_ocp();
	int get_age();
	vector<scrd> scored;
private:
	string username;
	string password;
	int age;
	string uccupation;
	bool athlete;
	bool signed_in;
};
#endif