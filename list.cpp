#include <iostream>
#include "list.h"
using namespace std;
void Node::print(){
	G->print();
}
Node* List::head() { 
	return _head; 
}
Node* List::last() {
 return _last; 
}
List::List() {
    _head = NULL;
    _last = NULL;
}
void List::push_front(game* x) {
    Node *new_node = new Node(x);
    new_node->next = _head;
    if (_head != NULL)
        _head->prev = new_node;
    _head = new_node;
    if (_last == NULL)
        _last = new_node;
}

void List::push_back(game* x) {
    if (_head == NULL)
        push_front(x);
    else {
        Node *new_node = new Node(x);
        new_node->prev = _last;
        _last->next = new_node;
        _last = new_node;
    }
}

List::~List() {
    clear();
}

void List::clear() {
    Node *p = _head;
    while (p != NULL) {
        Node *q = p;
        p = p->next;
        delete q;
    }
    _head = NULL;
    _last = NULL;
}

void high_ranked_games::print(int num){
	Node* p=head();
	for(int i=0;i<num-1;i++){
		p=p->next;
	}
	p->G->print();
}
void high_ranked_games::sort(){
	for(Node* p=_head->next;p!=NULL;p=p->next){
		for(Node* m=_head;m!=p;m=m->next){
			if(m->G->get_score()<p->G->get_score()){
				if(m->next!=p){
					Node* temp1=new Node(p->G);
					Node* temp2=new Node(m->G);
					temp1->next=m->next;
					temp1->prev=m->prev;
					if(m->prev !=NULL){
						m->prev->next=temp1;
					}
					else{
						_head=temp1;
					}
					m->next->prev=temp1;
					temp2->next=p->next;
					temp2->prev=p->prev;
					p->prev->next=temp2;
					if(p->next !=NULL){
						p->next->prev=temp2;
					}
					else{
						_last=temp2;
					}
					m=temp1;
					if(temp1==_head){
						_head=m;
					}
					if(temp1==_last)
						_last=m;
					p=temp2;
					if(temp2==_head)
						_head=p;
					if(temp2==_last)
						_last=p;
				}else{
					Node* temp=new Node(p->G);
					temp->next=m;
					temp->prev=m->prev;
					m->next=p->next;
					if(m->prev !=NULL){
						m->prev->next=temp;
					}
					else{
						_head=temp;
					}
					m->prev=temp;
					if(p->next !=NULL){
						p->next->prev=m;
					}
					else{
						_last=m;
					}
					p=m;
					if(m==_head)
						_head=p;
					if(m==_last)
						_last=p;
					m=temp;
					if(temp==_head)
						_head=m;
					if(temp==_last)
						_last=m;
				}
			}
		}
	}
}

void newer_games::print(int num){
	Node* p=head();
	for(int i=0;i<num-1;i++){
		p=p->next;
	}
	p->G->print();
}
void newer_games::sort(){
	for(Node* p=_head->next;p!=NULL;p=p->next){
		for(Node* m=_head;m!=p;m=m->next){
			if(m->G->get_year()<p->G->get_year()){
				if(m->next!=p){
					Node* temp1=new Node(p->G);
					Node* temp2=new Node(m->G);
					temp1->next=m->next;
					temp1->prev=m->prev;
					if(m->prev !=NULL){
						m->prev->next=temp1;
					}
					else{
						_head=temp1;
					}
					m->next->prev=temp1;
					temp2->next=p->next;
					temp2->prev=p->prev;
					p->prev->next=temp2;
					if(p->next !=NULL){
						p->next->prev=temp2;
					}
					else{
						_last=temp2;
					}
					m=temp1;
					if(temp1==_head){
						_head=m;
					}
					if(temp1==_last)
						_last=m;
					p=temp2;
					if(temp2==_head)
						_head=p;
					if(temp2==_last)
						_last=p;
				}else{
					Node* temp=new Node(p->G);
					temp->next=m;
					temp->prev=m->prev;
					m->next=p->next;
					if(m->prev !=NULL){
						m->prev->next=temp;
					}
					else{
						_head=temp;
					}
					m->prev=temp;
					if(p->next !=NULL){
						p->next->prev=m;
					}
					else{
						_last=m;
					}
					p=m;
					if(m==_head)
						_head=p;
					if(m==_last)
						_last=p;
					m=temp;
					if(temp==_head)
						_head=m;
					if(temp==_last)
						_last=m;
				}
			}
		}
	}
}

void most_rated_games::print(int num){
	Node* p=head();
	for(int i=0;i<num-1;i++){
		p=p->next;
	}
	p->G->print();
}
void most_rated_games::sort(){
	for(Node* p=_head->next;p!=NULL;p=p->next){
		for(Node* m=_head;m!=p;m=m->next){
			if(m->G->get_rated_by()<p->G->get_rated_by()){
				if(m->next!=p){
					Node* temp1=new Node(p->G);
					Node* temp2=new Node(m->G);
					temp1->next=m->next;
					temp1->prev=m->prev;
					if(m->prev !=NULL){
						m->prev->next=temp1;
					}
					else{
						_head=temp1;
					}
					m->next->prev=temp1;
					temp2->next=p->next;
					temp2->prev=p->prev;
					p->prev->next=temp2;
					if(p->next !=NULL){
						p->next->prev=temp2;
					}
					else{
						_last=temp2;
					}
					m=temp1;
					if(temp1==_head){
						_head=m;
					}
					if(temp1==_last)
						_last=m;
					p=temp2;
					if(temp2==_head)
						_head=p;
					if(temp2==_last)
						_last=p;
				}else{
					Node* temp=new Node(p->G);
					temp->next=m;
					temp->prev=m->prev;
					m->next=p->next;
					if(m->prev !=NULL){
						m->prev->next=temp;
					}
					else{
						_head=temp;
					}
					m->prev=temp;
					if(p->next !=NULL){
						p->next->prev=m;
					}
					else{
						_last=m;
					}
					p=m;
					if(m==_head)
						_head=p;
					if(m==_last)
						_last=p;
					m=temp;
					if(temp==_head)
						_head=m;
					if(temp==_last)
						_last=m;
				}
			}
		}
	}
}