#include <iostream>
#include "game.h"
using namespace std;
void game::set_score(int s,user usr){
	if(genre=="sports" && usr.get_ocp()==false){
		score+=2*s;
	}
	else if(genre=="action" && usr.get_age()>30){
		score+=2*s;
	}
	else if(genre=="puzzle" && usr.get_age()<15){
		score+=3*s;
	}
	else score+=s;
}
void game::print(){
	cout<<"Game: <"<<name<<">"<<endl;
	cout<<"Year: <"<<year<<">"<<endl;
	cout<<"Genre: <"<<genre<<">"<<endl;
	cout<<"Score: <"<<score<<">"<<endl;
	cout<<"Rated by: <"<<rated_by<<">"<<endl;
}
game::game(string _name,int _year,string _genre,int _score,int _num){
	name=_name;
	year=_year;
	genre=_genre;
	score=_score;
	rated_by=_num;
}
int game::get_score(){
	return score;
}
int game::get_year(){
	return year;
}
int game::get_rated_by(){
	return rated_by;
}
string game::get_name(){
	return name;
}