#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include "list.h"
using namespace std;
int to_int(string s){
	int result=0;
	for(int i=s.size()-1;i>=0;i--){
		result+=(s[i]-'0')*pow(10,s.size()-1-i);
	}
	return result;
}
game read_line(string in){
	string name,genre;
	int year,score,rated_by;
	int count=0,begin=0;
	while(in[count]!=','){
		count++;
	}
	name=in.substr(begin,count);
	count++;
	while(in[count]!=' '){
		count++;
	}
	begin=count+1;
	while(in[count]!=','){
		count++;
	}
	year=to_int(in.substr(begin,count-begin));
	count++;
	while(in[count]!=' '){
		count++;
	}
	begin=count+1;
	while(in[count]!=','){
		count++;
	}
	genre=in.substr(begin,count-begin);
	count++;
	while(in[count]!=' '){
		count++;
	}
	begin=count+1;
	while(in[count]!=','){
		count++;
	}
	score=to_int(in.substr(begin,count-begin));
	count++;
	while(in[count]!=' '){
		count++;
	}
	begin=count+1;
	while(count<in.size()){
		count++;
	}
	rated_by=to_int(in.substr(begin,count-begin));
	return game(name,year,genre,score,rated_by);
}

vector<string> _input(string s){
	vector<string> strs;
	int counter=0,j=0,flag=0;
	while(j<s.size()){
		while(s[j]!=' ' && j<s.size()){
			counter++;
			j++;
			if(s[j]==' ')
				flag=1;
		}	
		strs.push_back(s.substr(j-counter,counter));
		if(flag==1){
			while(s[j]==' '){
				j++;
			}
		}
		counter=0;
	}
	return strs;
}
int main(){
	fstream f;
	vector<user> users;
	string in;
	int count,flag=0;
	vector<string> input;
	f.open("games.info");
	while (getline (f,in))
		input.push_back(in);
	f.close();
	vector<game> games;
	for(int i=0;i<input.size();i++){
		games.push_back(read_line(input[i]));
	}
	user usr;
	newer_games n;
	high_ranked_games h;
	most_rated_games m;
	bool signin=false;
	while(getline(cin,in)){
		List* l;
		int flag=0;
		vector<string> inp=_input(in);
		if(inp.size()==5 && inp[0]=="sign_up"){
			users.push_back(user(inp[1],inp[2],to_int(inp[3]),inp[4],games));
		}
		if(inp.size()==3 && inp[0]=="sign_in"){
			signin=true;
			int i;
			for(i=0;i<users.size();i++){
				if(users[i].get_name()==inp[1])
					break;
			}
			if(i==users.size())
				cout<<"username not found!"<<endl;
			else if(!users[i].login(inp[1],inp[2]))
				cout<<"wrong password!"<<endl;
			else{
				for(int j=0;j<users.size();j++)
					users[j].set_signed_in(false);
				users[i].set_signed_in(true);
			}
			usr=users[i];
		}
		if(inp.size()==3 && inp[0]=="score"){
			if(signin==false){
				cout<<"sign in first!"<<endl;
				continue;
			}
			int i;
			for(i=0;i<games.size();i++){
				if(inp[1]==games[i].get_name())
					break;
			}
			if(i==games.size()){
				cout<<"game not found!"<<endl;
				continue;
			}
			else if(to_int(inp[2])>3 || to_int(inp[2])<0){
				cout<<"score is out of range!"<<endl;
				continue;
			}
			else if(usr.scored_before(games[i].get_name())){
				cout<<"you gived score before! your new score will be calculated."<<endl;
				games[i].set_score(-1*(usr.get_past_score(games[i].get_name())),usr);
				games[i].set_score(to_int(inp[2]),usr);
				usr.set_past_score(games[i].get_name(),to_int(inp[2]));
			}
			else{
				usr.set_past_score(games[i].get_name());
				games[i].set_score(to_int(inp[2]),usr);
				usr.set_past_score(games[i].get_name(),to_int(inp[2]));
			}
		}
		if(inp.size()==4 && inp[0]=="add_game"){
			if(signin==false){
				cout<<"sign in first!"<<endl;
				continue;
			}
			if(inp[3]!="sports" && inp[3]!="puzzle" && inp[3]!="action"){
				cout<<"genre is out of range!"<<endl;
				continue;
			}
			game new_game(inp[1],to_int(inp[2]),inp[3],0,0);
			games.push_back(new_game);
			scrd temp;
			temp.game_name=inp[1];
			temp.scored=false;
			temp.score=0;
			for(int i=0;i<users.size();i++){
				users[i].scored.push_back(temp);
			}
		}
		if(inp.size()==2 && inp[0]=="remove_game"){
			if(signin==false){
				cout<<"sign in first!"<<endl;
				continue;
			}
			int i;
			for(i=0;i<games.size();i++){
				if(games[i].get_name()==inp[1])
					break;
			}
			if(i==games.size()){
				cout<<"game not found!"<<endl;
				continue;
			}
			games.erase(games.begin()+i);
			for(int k=0;k<users.size();k++){
				int j;
				for(j=0;j<users[k].scored.size();j++){
					if(users[k].scored[j].game_name==inp[1])
						break;
				}
				users[k].scored.erase(users[k].scored.begin()+j);
			}
		}
		if(inp.size()==2 && inp[0]=="view_list" && inp[1].substr(0,inp[1].size())=="high_ranked_games"){
			if(signin==false){
				cout<<"sign in first!"<<endl;
				continue;
			}
			flag=1;
			count=1;
			l=&h;	
			for(int i=0;i<games.size();i++){
				l->push_back(&games[i]);
			}
		}
		if(inp.size()==2 && inp[0]=="view_list" && inp[1].substr(0,inp[1].size())=="newer_games"){
			if(signin==false){
				cout<<"sign in first!"<<endl;
				continue;
			}
			flag=1;
			count=1;
			l=&n;
			for(int i=0;i<games.size();i++){
				l->push_back(&games[i]);
			}
		}
		if(inp.size()==2 && inp[0]=="view_list" && inp[1].substr(0,inp[1].size())=="most_rated_games"){
			if(signin==false){
				cout<<"sign in first!"<<endl;
				continue;
			}
			flag=1;
			count=1;
			l=&m;
			for(int i=0;i<games.size();i++){
				l->push_back(&games[i]);
			}
		}
		if(inp.size()==1 && inp[0]=="next_game"){
			if(signin==false){
				cout<<"sign in first!"<<endl;
				continue;
			}
			flag=1;
			if(count==games.size()-1){
				cout<<"not anymore games!"<<endl<<endl;
				continue;
			}
			else
				count++;
		}
		if(inp.size()==1 && inp[0]=="previous_game"){
			if(signin==false){
				cout<<"sign in first!"<<endl;
				continue;
			}
			flag=1;
			if(count==1){
				cout<<"not anymore games!"<<endl;
				continue;
			}
			else
				count--;
		}
		if(flag==1){
			l->sort();
			l->print(count);
			flag=0;
		}
	}
	return 0;
}