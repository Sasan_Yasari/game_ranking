#ifndef LIST_H
#define LIST_H
#include <iostream>
#include <vector>
#include "game.h"
using namespace std;
class Node{
public:
	Node(game* g, Node *n = NULL, Node *p = NULL)
        :G(g),next(n),prev(p){}
    void print();
	game* G;
	Node* next;
	Node* prev;
};
class List {
public:
    List();
    ~List();
    virtual void sort()=0;
    virtual void print(int)=0;
    void push_front(game* x);
    void push_back(game* x);   
    Node* head();
    Node* last();
    void clear();
protected:
    Node* _head;
    Node* _last;
};
class high_ranked_games:public List{
public:
	void print(int);
	void sort();
};
class newer_games:public List{
	public:
	void print(int);
	void sort();
};
class most_rated_games:public List{
public:
	void print(int);
	void sort();
};
#endif