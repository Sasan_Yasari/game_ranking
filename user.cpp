#include <iostream>
#include <vector>
#include "user.h"
using namespace std;
string user::get_name(){
	return username;
}
bool user::get_ocp(){
	return athlete;
}
int user::get_age(){
	return age;
}
void user::set_past_score(string name){
	int i;
	for(i=0;i<scored.size();i++){
		if(scored[i].game_name==name)
			break;
	}
	scored[i].scored=true;
}
void user::set_past_score(string name,int s){
	int i;
	for(i=0;i<scored.size();i++){
		if(scored[i].game_name==name)
			break;
	}
	scored[i].score=s;
}
int user::get_past_score(string name){
	int i;
	for(i=0;i<scored.size();i++){
		if(scored[i].game_name==name)
			break;
	}
	return scored[i].score;
}
bool user::scored_before(string name){
	int i;
	for(i=0;i<scored.size();i++){
		if(scored[i].game_name==name)
			break;
	}
	return scored[i].scored;
}
void user::set_signed_in(bool b){
	signed_in=b;
}
user::user(){
	username="";
	password="";
	uccupation="";
	age=0;
	athlete=false;
	signed_in=false;
}
user::user(string user,string pass,int _age,string ucp,vector<game> games){
	username=user;
	password=pass;
	age=_age;
	uccupation=ucp;
	if(ucp=="athlete")
		athlete=true;
	else athlete=false;
	signed_in=false;
	for(int i=0;i<games.size();i++){
		scrd temp;
		temp.game_name=games[i].get_name();
		temp.scored=false;
		temp.score=0;
		scored.push_back(temp);
	}
}
bool user::login(string name,string pass){
	if(name==username && pass==password)
		return true;
	return false;
}